import collections
import time
API_BATCH_SIZE = 50

class BatchRequest():
    def __init__(self, service, targets, req_lambda, batch_size=API_BATCH_SIZE):
        self._service = service
        self.queue = collections.deque(targets)
        self.rerun_queue = collections.deque()
        self.req_lambda = req_lambda
        self.batch_size = batch_size

        self.is_completed = False
        self.error_accumulator = []
        self.unrecoverable_errors = []
        self.results = []

        self.back_off = False
        self.backoff_power = 0


    def execute(self):
        if self.is_completed:
            raise Exception("This request is already completed")

        self.run_queue()
        while self.rerun_queue:
            print("Some requests must be rerun")
            self.queue = self.rerun_queue
            self.rerun_queue = collections.deque()
            self.run_queue()

        self.is_completed = True
        return (self.results, self.unrecoverable_errors)

    def run_queue(self):
        batch = self._service.new_batch_http_request()
        i = 0
        for target in self.queue:
            i += 1
            batch.add(
                self.req_lambda(target),
                lambda id, r, e: self.process_result(id, r, e))
            if i == self.batch_size:
                print(f"Running batch of {self.batch_size} ({len(self.queue)} msgs left)")
                batch.execute()
                batch = self._service.new_batch_http_request()
                i = 0
            self.process_errors()
            self.process_backoff()
        # Execute last batch if it contains outstanding requests
        if i > 0:
            batch.execute()


    def process_result(self, target, res, exception):
        if res is None:
            print(exception)
            self.error_accumulator.append({"target": target, "err": exception})
            # Retry when hit request limit
            # TODO: 403 and quota exceeded messages should be handled here
        else:
            self.results.append(res)

    def process_errors(self):
        err_429_encountered = False
        for err in self.error_accumulator:
            target = err["target"]
            exception = err["err"]
            if exception.resp.status == 429 and not err_429_encountered:
                print(exception)
                err_429_encountered = True
                self.back_off = True
                self.rerun_queue.append(target)
                # error 429 means batch is too large. Halve it and try again
                if self.batch_size > 1:
                    print(f"batch size {self.batch_size} is too large. Halving it")
                    self.batch_size = int(self.batch_size / 2)
            else:
                self.unrecoverable_errors.append(err)

        if not err_429_encountered:
            self.back_off = False
            self.backoff_power = 0

    def process_backoff(self):
        if self.back_off:
            self.backoff_power += 1
            wait_time = 2 ** self.backoff_power
            print(f"backoff is triggered, waiting for {wait_time} seconds")
            time.sleep(wait_time)
            # TODO: give up if wait_time is too long

        else:
            self.backoff_power = 0
                
