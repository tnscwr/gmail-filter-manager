#!/usr/bin/python3
import os
import flask
import requests
import functools

import google.oauth2.credentials
import google_auth_oauthlib.flow
import googleapiclient.discovery

import label_manager
import entity_types

CLIENT_SECRETS_FILE = "client_secret.json"

SCOPES = ['https://www.googleapis.com/auth/gmail.readonly', 'https://www.googleapis.com/auth/gmail.modify', 'https://www.googleapis.com/auth/gmail.settings.basic']
API_SERVICE_NAME = 'gmail'
API_VERSION = 'v1'

app = flask.Flask(__name__)
app.secret_key = os.urandom(24)

def check_session(func):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        if 'credentials' not in flask.session:
            return flask.redirect('authorize')
        credentials = google.oauth2.credentials.Credentials(**flask.session['credentials'])
        res = func(*args, **kwargs)
        # Save credentials back to session in case access token was refreshed.
        # ACTION ITEM: In a production app, you likely want to save these
        #              credentials in a persistent database instead.
        flask.session['credentials'] = credentials_to_dict(credentials)
        return res
    return wrapper


@app.teardown_request
def flush_session(req):
    db_session=entity_types.get_session()
    db_session.flush()
    return req


@app.route('/')
@check_session
def index():
    #return flask.render_template('index.html')
    return flask.redirect(flask.url_for('internals'))


@app.route('/internals')
@check_session
def internals():
    return flask.render_template('internals.html')


@app.route('/filters')
def get_filters():
    mgr = label_manager.LabelManager(db_session=entity_types.get_session())
    fltrs = mgr.get_filters()
    return flask.render_template('filters.html', filters=fltrs)


@app.route('/apply_filter')
@check_session
def apply_filter():
    mgr = label_manager.LabelManager(db_session=entity_types.get_session())
    fltr = flask.request.args.get("filter")
    mgr.apply_filter(fltr=fltr)
    return flask.redirect('/filters')


@app.route('/labels')
def get_labels():
    mgr = label_manager.LabelManager(db_session=entity_types.get_session())
    labels = mgr.get_labels()
    return flask.render_template('labels.html', labels=labels)


@app.route('/assignments')
def get_assignments():
    mgr = label_manager.LabelManager(db_session=entity_types.get_session())
    target_label=flask.request.args.get("label")
    emails = mgr.get_assignments(target_label=target_label)
    return flask.render_template('assign_labels.html', emails=emails, target_label=target_label)


@app.route('/fetch_senders_by_label', methods=["GET", "POST"])
@check_session
def fetch_senders_by_label():
    mgr = label_manager.LabelManager(db_session=entity_types.get_session())
    target_label = flask.request.args.get("label")
    res = mgr.fetch_senders_by_label(target_label )
    #TODO: flash number of added emails here
    return flask.redirect(f'/assignments?label={target_label}')


@app.route('/fetch_filters', methods=["GET", "POST"])
@check_session
def fetch_filters():
    mgr = label_manager.LabelManager(db_session=entity_types.get_session())
    mgr.fetch_filters()
    return flask.redirect(flask.url_for('get_filters'))


@app.route('/fetch_labels', methods=["GET", "POST"])
@check_session
def fetch_labels():
    mgr = label_manager.LabelManager(db_session=entity_types.get_session())
    labels = mgr.fetch_labels()
    return flask.redirect(flask.url_for('get_labels'))


@app.route('/push_senders_by_label', methods=["GET", "POST"])
@check_session
def push_senders_by_label():
    mgr = label_manager.LabelManager(db_session=entity_types.get_session())
    target_label = flask.request.args.get("label")
    senders = mgr.get_assignments(target_label=target_label, unassigned_only=True)
    senders_list = [sndr.address for sndr in senders]
    mgr.update_filters(target_label, senders_list)
    mgr.fetch_filters()
    return flask.redirect(f'/assignments?label={target_label}')


@app.route('/authorize')
def authorize():
    # Create flow instance to manage the OAuth 2.0 Authorization Grant Flow steps.
    flow = google_auth_oauthlib.flow.Flow.from_client_secrets_file(
        CLIENT_SECRETS_FILE, scopes=SCOPES)

    flow.redirect_uri = flask.url_for('oauth2callback', _external=True)

    authorization_url, state = flow.authorization_url(
        access_type='offline',
        include_granted_scopes='true')

    # Store the state so the callback can verify the auth server response.
    flask.session['state'] = state
    flask.session['code_verifier'] = flow.code_verifier

    return flask.redirect(authorization_url)


@app.route('/oauth2callback')
def oauth2callback():
    # Specify the state when creating the flow in the callback so that it can
    # verified in the authorization server response.
    state = flask.session['state']

    flow = google_auth_oauthlib.flow.Flow.from_client_secrets_file(
        CLIENT_SECRETS_FILE, scopes=SCOPES, state=state)
    flow.redirect_uri = flask.url_for('oauth2callback', _external=True)
    flow.code_verifier = flask.session.get('code_verifier')

    # Use the authorization server's response to fetch the OAuth 2.0 tokens.
    authorization_response = flask.request.url
    flow.fetch_token(authorization_response=authorization_response)

    # Store credentials in the session.
    # ACTION ITEM: In a production app, you likely want to save these
    #              credentials in a persistent database instead.
    credentials = flow.credentials
    flask.session['credentials'] = credentials_to_dict(credentials)

    return flask.redirect(flask.url_for('index'))


@app.route('/revoke')
def revoke():
    if 'credentials' not in flask.session:
        return ('You need to <a href="/authorize">authorize</a> before ' +
            'testing the code to revoke credentials.')

    credentials = google.oauth2.credentials.Credentials(
        **flask.session['credentials'])

    revoke = requests.post('https://accounts.google.com/o/oauth2/revoke',
        params={'token': credentials.token},
        headers = {'content-type': 'application/x-www-form-urlencoded'})

    status_code = getattr(revoke, 'status_code')
    if status_code == 200:
        return flask.render_template('index.html', msg='Credentials successfully revoked.')
    else:
        return flask.render_template('index.html', msg='An error occurred.')


@app.route('/clear')
def clear_credentials():
    if 'credentials' in flask.session:
        del flask.session['credentials']
    return flask.render_template('index.html', msg='Credentials have been cleared.')


def credentials_to_dict(credentials):
    return {'token': credentials.token,
        'refresh_token': credentials.refresh_token,
        'token_uri': credentials.token_uri,
        'client_id': credentials.client_id,
        'client_secret': credentials.client_secret,
        'scopes': credentials.scopes}


if __name__ == '__main__':
    os.environ['OAUTHLIB_INSECURE_TRANSPORT'] = '1'

    entity_types.init_db()
    app.run('localhost', 8080, debug=True)
