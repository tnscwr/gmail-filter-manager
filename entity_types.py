from sqlalchemy import create_engine, Column, Integer, String, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, sessionmaker
from sqlalchemy.inspection import inspect

Base = declarative_base()
Session = sessionmaker()

class BaseEntity(Base):
    __abstract__ = True
    @classmethod
    def get_or_create(cls, session, **kwargs):
        pk = inspect(cls).primary_key[0].name
        inst = session.query(cls).get(kwargs[pk])
        if inst is None:
            inst = cls(**kwargs)
            session.merge(inst)
            session.commit()
        return inst

class Email(BaseEntity):
    __tablename__ = 'email_addresses'

    address = Column(String, nullable=False, primary_key=True)
    target_label_id = Column(Integer, ForeignKey('labels.gid'))
    filter_id = Column(Integer, ForeignKey('filters.gid'))

    target_label = relationship("Label", back_populates="emails")
    target_filter = relationship("Filter", back_populates="emails")

class Label(BaseEntity):
    __tablename__ = 'labels'

    gid = Column(String, primary_key=True)
    name = Column(String, nullable=False)

    emails = relationship("Email", back_populates="target_label")
    filters = relationship("Filter", back_populates="target_label")

class Filter(BaseEntity):
    __tablename__ = 'filters'

    gid = Column(String, primary_key=True)
    target_label_id = Column(String, ForeignKey('labels.gid'))
    managed = Column(Integer)

    target_label = relationship("Label", back_populates="filters")
    emails = relationship("Email", back_populates="target_filter")

def init_db():
    engine = create_engine('sqlite:///label_manager.db', echo=True)
    Session.configure(bind=engine)
    Base.metadata.create_all(engine)

def get_session():
    session = Session()
    return session
