import flask
import time
import email.utils
from entity_types import Email, Label, Filter
import google.oauth2.credentials
import googleapiclient.discovery
import functools
from batch_request import BatchRequest

API_SERVICE_NAME = 'gmail'
API_VERSION = 'v1'

# Google recommends batch size not over 50
API_BATCH_SIZE = 50


# TODO: data classes
# type hints
# Logging
#
# Per-label settings like 'Never send to spam'
class LabelManager():
    _db = None
    def __init__(self, db_session=None):
        credentials = flask.session.get('credentials')
        if credentials:
            self._service = googleapiclient.discovery.build(API_SERVICE_NAME, API_VERSION,
                credentials=google.oauth2.credentials.Credentials(**credentials))
        LabelManager._db = db_session
        self._db = db_session

    # The user may delete some records on server since previous fetch. Need to detect that records from the DB
    # NOTE: func MUST return list of IDs it fetched
    #       Model must have fileld gid as Google identifier.
    #       This decorator isn't fit for Email model because emails are fetched only partially (i.e. per label)
    def check_obsolete(model):
        def wrap(func):
            @functools.wraps(func)
            def wrapper(*args, **kwargs):
                old_ids = set([o.gid for o in LabelManager._db.query(model).all()])
                new_ids = set(func(*args, **kwargs))
                obsolete_ids = list(old_ids - new_ids)
                for obj in LabelManager._db.query(model).filter(model.gid.in_(obsolete_ids)):
                    LabelManager._db.delete(obj)
                LabelManager._db.commit()
            return wrapper
        return wrap

    @check_obsolete(Filter)
    def fetch_filters(self):
        filters = self._service.users().settings().filters().list(userId='me').execute()
        res = []
        for fltr in filters['filter']:
            if _is_filter_manageable(fltr):
                target_label = Label.get_or_create(self._db, gid=fltr["action"]["addLabelIds"][0])
                filter_obj = Filter.get_or_create(self._db, gid=fltr["id"], target_label=target_label)
                res.append(filter_obj.gid)
                for addr in fltr["criteria"]["from"].split(" OR "):
                    email = Email.get_or_create(self._db, address=addr, target_label=target_label)
                    filter_obj.emails.append(email)
                self._db.merge(filter_obj)
        return res

    @check_obsolete(Label)
    def fetch_labels(self):
        resp = self._service.users().labels().list(userId='me').execute()
        labels = resp['labels']

        res = []
        for label in labels:
            label_obj = Label(gid=label['id'], name=label['name'])
            res.append(label_obj.gid)
            self._db.merge(label_obj)
        self._db.commit()
        return res

    def fetch_senders_by_label(self, label_id, limit=None):
        msgids = []
        for msg in _get_paginated(self._service.users().messages().list, page_count=4, userId='me', labelIds=[label_id]):
            msgids.append(msg["id"])

        batch = BatchRequest(
                self._service,
                msgids,
                lambda msgid: self._service.users().messages().get(userId='me', id=msgid, format='metadata')
            )
        results, errors = batch.execute()

        senders = set()
        for res in results:
            senders.add(_extract_sender(res))

        target_label = Label.get_or_create(self._db, gid=label_id)
        for addr in senders:
            email = Email.get_or_create(self._db, address=addr, target_label=target_label)
        self._db.commit()

    # One cannot update existing filter (only delete and create),
    # so old filter will be destroyed and new one created instead
    # This way we can minimize number of filters in use
    #
    # Alternatively, filter length appears to be enforced in UI, but not in API. We can exploit it,
    # but such filter won't be editable manually (and this behaviour can be suddenly fixed)
    def update_filters(self, target_label, senders):
        # We try to keep number of filters down.
        # To do that, we take least populated filter and add its emails to the pool, then delete this filter
        current_filters = self.get_filters(target_label=target_label)
        shortest_filter = None
        for fltr in current_filters:
            if shortest_filter is None:
                shortest_filter = fltr
            if len(fltr.emails) < len(shortest_filter.emails):
                shortest_filter = fltr

        delete_shortest = False
        all_senders = senders
        # As useful coincidence, filter condition can hold slightly over 50 emails, same as batch size recommended by Google
        if shortest_filter is not None and len(shortest_filter.emails) < API_BATCH_SIZE:
            previous_senders = list(map(lambda sndr: sndr.address, shortest_filter.emails))
            all_senders = [*senders, *previous_senders]
            delete_shortest = True
        for chunk in ([all_senders[pos : pos + API_BATCH_SIZE] for pos in range(0, len(all_senders), API_BATCH_SIZE)]):
            filter_id = self.push_filter(target_label, chunk)
            # Don't delete anything if there was an error
            if filter_id is None: delete_shortest=False
        # FIXME: this code is way too optimistic
        if delete_shortest:
            self.delete_filter(shortest_filter.gid)

    def push_filter(self, target_label, senders):
        if target_label is None:
            raise Exception("You must supply target label")
        if len(senders) < 1:
            raise Exception("You must supply sender list")

        filter_structure = {
            "criteria": {
                "from": ' OR '.join(senders)
            },
            'action': {
                'addLabelIds': [target_label],
                'removeLabelIds': ['INBOX']
            }
        }
        # TODO: check for errors
        result = self._service.users().settings().filters().create(userId='me', body=filter_structure).execute()
        return result.get('id')

    def apply_filter(self, fltr, source_label='INBOX'):
        filter_obj = self._db.query(Filter).get(fltr)
        senders_query = ' OR '.join([e.address for e in filter_obj.emails])
        msgids = []
        for msg in _get_paginated(
                                  self._service.users().messages().list,
                                  userId='me',
                                  labelIds=[source_label],
                                  q=f'from:({senders_query})'):
            msgids.append(msg["id"])

        request_body = {
            'addLabelIds': [filter_obj.target_label.gid],
            'removeLabelIds': [source_label]
        }

        batch = BatchRequest(
                self._service,
                msgids,
                lambda msgid: self._service.users().messages().modify(userId='me', id=msgid, body=request_body),
                batch_size=25,
            )
        results, errors = batch.execute()


    def delete_filter(self, gid):
        self._service.users().settings().filters().delete(userId='me', id=gid).execute()

    def get_filters(self, target_label=None):
        q = self._db.query(Filter)
        if target_label is not None:
            q = q.filter(Filter.target_label_id == target_label)
        return q.all()

    def get_labels(self):
        return self._db.query(Label).all()

    # target_label means return only emails belonging to specified label
    # unassigned_only means only emails not assigned to a filter will be returned
    def get_assignments(self, target_label=None, unassigned_only=False):
        q = self._db.query(Email)
        if target_label is not None:
            q = q.filter(Email.target_label_id == target_label)
        if unassigned_only:
            q = q.filter(Email.filter_id.is_(None))
        return(q.all())


# This app uses specific template for filters it can manage:
#   Criteria is only "from"
#   Actions are only addLabelIds and removeLabelIds
#   removeLabelIds removes only INBOX
#   addLabelIds adds only one label
#   email address in from: filter doesn't contain asterisk (*) or other special character (if any)
#   TODO: we should handle these metacharacters somehow. Otherwise we'll end up creating redundand filters
#   for these expressions because individual addresses will show up as unfiltered
def _is_filter_manageable(fltr):
    if set(fltr["criteria"].keys()) != {"from"}: return False
    if "*" in fltr["criteria"]["from"]: return False
    # FIXME: "never send to spam" may or may not be here
    if set(fltr["action"].keys()) != {"addLabelIds", "removeLabelIds"}: return False
    if len(fltr["action"]["addLabelIds"]) != 1: return False
    if len(fltr["action"]["removeLabelIds"]) != 1: return False
    if fltr["action"]["removeLabelIds"][0] != "INBOX": return False
    return True

# You cannot just repeat Google API request with amended parameters.
# Therefore, you must pass desired method of your resource and its params separately,
# so this method could build request to next page
def _get_paginated(resource_method, page_count=float('inf'), **request_params):
    page_token = False
    pages = 0
    while page_token is not None and pages <= page_count:
        print(f"fetching page {pages} of {page_count}")
        pages += 1
        if page_token:
            request_params["pageToken"] = page_token
        results = resource_method(**request_params).execute()
        msgs = results.get('messages', [])
        page_token = results.get('nextPageToken')
        for msg in msgs:
            yield msg

def _extract_sender(msg):
    for header in msg["payload"]["headers"]:
        if header['name'] == "From":
            print(header["value"])
            return email.utils.parseaddr(header["value"])[1]

